﻿using System;
using System.Collections.Generic;

namespace SortableShapes
{
    static class Program
    {
        static void Main(string[] args)
        {
            var side = 5;
            var radius = 2;
            var @base = 10;
            var height = 2;
            
            var shapes = new List<Shape>{ new Square(side),
                new Circle(radius),
                new Triangle(@base, height) };
            shapes.Sort();
            shapes.ForEach(Console.WriteLine);
        }
    }
}