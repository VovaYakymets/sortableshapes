﻿using System;

namespace SortableShapes
{
    public abstract  class Shape: IComparable<Shape>
    {
        protected abstract double GetArea();

        public int CompareTo(Shape other) => GetArea().CompareTo(other.GetArea());
    }
}