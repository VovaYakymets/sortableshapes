﻿namespace SortableShapes
{
    public class Square : Shape
    {
        private readonly double _side;

        public Square(double side)
        {
            _side = side;
        }

        protected override double GetArea() => _side * _side;
        
    }
}