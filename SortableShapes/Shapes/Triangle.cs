﻿namespace SortableShapes
{
    public class Triangle: Shape
    {
        private readonly double _base;
        private readonly double _height;

        public Triangle(double @base, double height)
        {
            _base = @base;
            _height = height;
        }

        protected override double GetArea() => _base * _height / 2;
    }
}