﻿using System;

namespace SortableShapes
{
    public class Circle: Shape
    {
        private readonly double _radius;

        public Circle(double radius)
        {
            _radius = radius;
        }

        protected override double GetArea() => _radius * _radius * Math.PI;
    }
}