﻿namespace SortableShapes
{
    public class Rectangle: Shape
    {
        private readonly double _width;
        private readonly double _height;

        public Rectangle(double width, double height)
        {
            _width = width;
            _height = height;
        }

        protected override double GetArea() => _height * _width;
    }
}